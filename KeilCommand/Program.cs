﻿using CommandLine;

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace KeilCommand
{
    public enum Commands
    {
        Build,
        Flash,
        Debug
    }
    public class App
    {
        private FileStream _LogStream = null;
        private StreamReader _LogReader = null;
        public String LogFile = null;
        private Thread _LogThread = null;
        private bool _Reading = false;

        public class Options
        {
            [Option('p', "project")]
            public String Project { get; set; }
            [Option('c', "command", HelpText = "The command to execute: Build, Flash, Debug")]
            public Commands Command { get; set; }
        }
        public void Run(String[] args)
        {
            LogFile = Path.GetTempFileName();
            String uv4 = @"c:\Keil_v5\UV4\UV4.exe";
            String arguments = String.Empty;
            Parser.Default.ParseArguments<Options>(args).WithParsed<Options>(o =>
            {
                String commandParam = String.Empty;
                switch (o.Command)
                {
                    case Commands.Flash:
                        commandParam = "-f -j0";
                        break;
                    case Commands.Debug:
                        commandParam = "-d";
                        break;
                    case Commands.Build:
                    default:
                        commandParam = "-b -j0";
                        break;
                }
                string project = o.Project;
                if (project == null)
                {
                    string[] files = new String[0];
                    var directory = Directory.GetCurrentDirectory();
                    do
                    {
                        files = Directory.GetFiles(directory, "*.uvprojx");
                        try
                        {
                            directory = Directory.GetParent(directory).FullName;
                        }
                        catch(Exception)
                        {
                            directory = null;
                        }
                    }
                    while (files.Length == 0 && directory != null);
                    if (files.Length == 0)
                    {
                        Console.Error.WriteLine("No project found");
                        Environment.Exit(1);
                    }
                    if (files.Length > 1)
                    {
                        Console.Error.WriteLine("More than one project found");
                        Environment.Exit(2);
                    }
                    project = files[0];
                }
                project = Path.GetFullPath(project);
                arguments = String.Format("{0} {1} -o {2}", commandParam, project, LogFile);
                using var process = new Process();
                process.StartInfo.FileName = uv4;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.Exited += buildExited;
                Console.WriteLine(String.Format("Starting {0} for {1}", o.Command.ToString(), project));
                _LogThread = new Thread(new ThreadStart(readTail));
                process.Start();
                Console.WriteLine(String.Format("Waiting"));
                _LogThread.Start();
                process.WaitForExit();
                _Reading = false;
                _LogThread.Join();
                readLog();
                dispose();
                File.Delete(LogFile);
            });
        }

        private void readTail()
        {
            if (_LogStream == null)
            {
                _LogStream = File.Open(LogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            if (_LogReader == null)
            {
                _LogReader = new StreamReader(_LogStream);
            }
            _Reading = true;
            while (_Reading)
            {
                readLog();
                Thread.Sleep(100);
            }
        }

        private void readLog()
        {
            string toEnd = _LogReader.ReadToEnd();
            if (toEnd.Length > 0)
            {
                Console.WriteLine(toEnd);
            }
        }

        private void buildExited(object sender, EventArgs e)
        {
            _LogThread.Abort();
            Console.WriteLine(String.Format("Done"));
        }

        private void dispose()
        {
            _LogReader.Dispose();
            _LogStream.Dispose();
        }
    }
    public class Program
    {
        public static void Main(string[] args)
        {
            var app = new App();
            app.Run(args);
        }

    }
}
